package pl.edu.pwsztar.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.chess.RulesOfGame;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.service.ChessService;

import java.util.Arrays;
import java.util.List;

@Service
public class ChessServiceImpl implements ChessService {

    private RulesOfGame bishop;
    private RulesOfGame knight;
    private RulesOfGame king;
    private RulesOfGame queen;
    private RulesOfGame rock;
    private RulesOfGame pawn;

    @Autowired
    public ChessServiceImpl(@Qualifier("Bishop") RulesOfGame bishop,
                            @Qualifier("Knight") RulesOfGame knight,
                            @Qualifier("King") RulesOfGame king,
                            @Qualifier("Queen") RulesOfGame queen,
                            @Qualifier("Rock") RulesOfGame rock,
                            @Qualifier("Pawn") RulesOfGame pawn) {
        this.bishop = bishop;
        this.knight = knight;
        this.king = king;
        this.queen = queen;
        this.rock = rock;
        this.pawn = pawn;
    }

    @Override
    public boolean isCorrectMove(FigureMoveDto figureMoveDto) {
        int startX;
        int startY;
        int destinationX;
        int destinationY;

        List<String> startPosition = Arrays.asList(figureMoveDto.getStart().split("_"));
        List<String> destinationPosition = Arrays.asList(figureMoveDto.getDestination().split("_"));

        startX = Integer.parseInt(startPosition.get(1));
        destinationX = Integer.parseInt(destinationPosition.get(1));
        startY = columnNameToInteger(startPosition.get(0));
        destinationY = columnNameToInteger(destinationPosition.get(0));

        switch(figureMoveDto.getType()){
            case BISHOP: {
                return bishop.isCorrectMove(startX, startY, destinationX, destinationY);
            }
            case KING: {
                return king.isCorrectMove(startX, startY, destinationX, destinationY);
            }
            case QUEEN: {
                return queen.isCorrectMove(startX, startY, destinationX, destinationY);
            }
            case PAWN: {
                return pawn.isCorrectMove(startX, startY, destinationX, destinationY);
            }
            case ROCK: {
                return rock.isCorrectMove(startX, startY, destinationX, destinationY);
            }
            case KNIGHT: {
                return knight.isCorrectMove(startX, startY, destinationX, destinationY);
            }
        }
        return false;

    }
    private int columnNameToInteger(String columnName){
        return columnName.charAt(0) - 96;
    }
}
